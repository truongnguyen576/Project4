import React from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getDataCV } from '../../state/actions';
import ListCVData from '../../components/ListCV';
import './ListCV.css';


function ListCV() {
    const listCv = useSelector(state => state.allcv.data);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDataCV());
    }, [])

    return (
        <div>
            <div className="list-cv">
                <h1>Danh sách kết quả cv</h1>
                <div className="data__cv">
                    <ListCVData dataCV={listCv} />
                </div>
                <div className="fiilter__cv">

                </div>
            </div>
        </div>
    )
}

export default ListCV
