import React from 'react'
import './CVItem.css';

function CvItem({itemCV}) {
    const {name,exp,intro,company,study,location,timework,salary,img,verify} = itemCV;
    return (
        <div className="cv__card">
            <img src={img}/>
            <div className="cv__info">
                <h2 className="name-cv">{name} {verify?<h4>(Verify)</h4>:<h5>(Not Verify)</h5>}</h2>
                <h4><i className="fa fa-briefcase"></i>{exp}</h4>
                <h4><i className="fa fa-briefcase"></i>{intro}</h4>
                <h4><i className="fa fa-briefcase"></i>{company}</h4>
                <h4><i className="fa fa-graduation-cap"></i>{study}</h4>
                <div className="info__border">
                    <h5>Địa chỉ : {location}</h5>
                    <h5>{timework}</h5>
                    <h5>{salary}</h5>
                </div>
            </div>
        </div>
    )
}

export default CvItem
