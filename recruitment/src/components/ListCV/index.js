import React from 'react'
import { Fragment } from 'react';
import CvItem from './CvItem';
import './ListCV.css';

function ListCV({dataCV}) {
    return (
         <Fragment>
             {dataCV?.map(item=><CvItem itemCV={item} key={item.id}/>)}
         </Fragment>
    )
}

export default ListCV
