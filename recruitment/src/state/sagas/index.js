import mySaga from './sagas';
import cvSaga from './cvSaga';
import { all } from 'redux-saga/effects';
export default function* rootSaga() {
    yield all([mySaga(),cvSaga()]);
  }
  