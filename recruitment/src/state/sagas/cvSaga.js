import { call, put, takeLatest, all } from 'redux-saga/effects';
import { Actions, getDataCvSuccess } from '../actions';
import TableService from '../../services/table.services';

function* fetchAllCV() {
    try {
      const allcv = yield call(TableService.getAllcv);
      yield put(getDataCvSuccess(allcv));
    } catch (e) {
      yield put({ type: 'USER_FETCH_FAILED', message: e.message });
    }
}

export default function* cvSaga() {
    yield takeLatest(Actions.GET_DATA_CV, fetchAllCV);
}
  