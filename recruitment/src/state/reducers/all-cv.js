import { Actions } from '../actions';

const initialState = {
  data: null,
};

const allCv = (state = initialState, action) => {
  switch (action.type) {
    case Actions.GET_DATA_CV_SUCCESS: {
      const { payload: data } = action;
      return {
        ...state,
        data,
      };
    }
    default:
      return state;
  }
};

export default allCv;
