import { combineReducers } from 'redux';
import allJobsReducer from './all-jobs';
import allCandidateReducer from './all-candidate';
import tokenExpReducer from './tokenExp';
import allCvReducer from './all-cv';

// Combine reducer
const rootReducer = combineReducers({
  allJobs: allJobsReducer,
  allCandidate: allCandidateReducer,
  tokenExpState: tokenExpReducer,
  allcv:allCvReducer
});

export default rootReducer;
